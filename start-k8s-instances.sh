#!/bin/bash

aws ec2 start-instances --instance-ids i-0d489563d8ab11b75 i-075cadc3bb3109bc5
sleep 10
instance0=$(aws ec2 describe-instances --instance-ids i-0d489563d8ab11b75 i-075cadc3bb3109bc5 |grep -m 1 PublicDnsName |cut -d '"' -f4)
instance1=$(aws ec2 describe-instances --instance-ids i-0d489563d8ab11b75 i-075cadc3bb3109bc5 | awk '/PublicDnsName/{i++}i==4' |grep PublicDnsName |cut -d '"' -f4)
sed -i "0,/ec2.*com/s//$instance0/" /home/ahalsey/.ssh/config
sed -i "0,/ec2.*com/! {0,/ec2.*com/s//$instance1/}" /home/ahalsey/.ssh/config
